import React, {memo} from "react";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import {MenuNav} from "../../components/Menu/Menu";
import {baseRoutes} from "../../constants/routes";

export const MenuAppBar = memo(({title}) => {
    return (
        <Toolbar>
            <MenuNav items={baseRoutes}>
                <MenuIcon/>
            </MenuNav>

            <h3>{title}</h3>
        </Toolbar>
    );
});
