import React from "react";

export const Header = ({children, className}) => {
    return (
        <div className={className}>
            {children}
        </div>
    )
};
