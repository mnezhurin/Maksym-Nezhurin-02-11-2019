import React, {
    Fragment
} from 'react';
import SearchInput from "../../components/Autocomplete/Autocomplete";

import {connect} from "react-redux";
import {addNewCode} from "../../actions";
import WeatherItem from "../../components/WeatherItem/WeatherItem";

const WeatherPage = ({addCity, matchKey}) => {
    if (matchKey !== -1)
        addCity({Key: matchKey})

    return (
        <Fragment>
            <p>
                Please, try to find the city or start type it name in this field for would have ability to see the weather there:
            </p>
            <SearchInput label="Enter the city" variant="outlined" onAddItem={addCity} />

            <WeatherItem />
        </Fragment>
    );
};

const mapStateToProps = (state, props) => ({
   matchKey: (props.match.params.id && state.codes.favorites.includes(parseInt(props.match.params.id, 10))) ?  parseInt(props.match.params.id, 10) : -1
});

const mapDispatchToProps = dispatch => ({
    addCity: ({Key}) => {
        dispatch(addNewCode(Key))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(WeatherPage)
