import React, {Fragment} from 'react';
import {connect} from "react-redux";
import FavoriteItem from "../../components/FavoriteItem/FavoriteItem";
import {
    Link,
} from "react-router-dom";

const FavoritePage = ({favorites}) => {
    return (
        <Fragment>
            <h2 className="text-center">Hello, user! You are on the favourite page!</h2>
            <p>On this page you can see your favorite city and follow weather changes in this city.</p>
            <div className="favoritesWrapper">
            {
                favorites ? favorites.map(code => <Link  key={code} to={`/${code}`}>
                        <FavoriteItem code={code} />
                    </Link>)
                    : (
                    <p>You have not any favorites items. Please make your choose!</p>
                )
            }
            </div>
        </Fragment>
    )
};

const mapStateToProps = state => ({
    favorites: state.codes.favorites
});

export default connect(mapStateToProps)(FavoritePage);