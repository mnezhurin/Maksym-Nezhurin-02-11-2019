import React, {useEffect} from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import {Header} from "./containers/Header/Header";
import {Footer} from "./containers/Footer/Footer";
import {MenuAppBar} from "./containers/AppBar/AppBar";
import {baseRoutes} from "./constants/routes";
import {
    withSnackbar,
    SnackbarProvider
} from "notistack";
import WeatherPage from "./pages/Weather/Weather";
import {connect} from "react-redux";
import "./App.css";

const App = ({message, enqueueSnackbar, variant}) => {
    const appTitle = "Weather Application";

    useEffect(() => {
        message && enqueueSnackbar(
            message,
            {variant}
        );
    }, [message]);

    return (
        <Router>
            <div className="application">
                <Header className="application-header">
                    <MenuAppBar title={appTitle}/>
                </Header>
                <div className="application-body">
                    <Switch>
                        {
                            Object.keys(baseRoutes).map( index => {
                                const route = baseRoutes[index];
                                return <Route
                                    key={route.path}
                                    exact
                                    path={route.path}
                                    component={route.component}
                                />
                            })
                        }
                        <Route path={'/:id'} component={WeatherPage}/>
                        <Redirect to="/"/>
                    </Switch>
                </div>
                <Footer className="application-footer">
                    Footer
                </Footer>
            </div>
        </Router>
    )
};

const mapStateToProps = state => ({
    message: state.informer.message,
    variant: state.informer.variant
});

const MyApp = withSnackbar(connect(mapStateToProps)(App));

export default function IntegrationNotistack() {
    return (
        <SnackbarProvider maxSnack={3}>
            <MyApp/>
        </SnackbarProvider>
    );
};
