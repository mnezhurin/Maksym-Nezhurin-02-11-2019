const apikey = 'rBvUOwJ0F1a6YC9g4W4Gu3SgqAlNE6d9',
    baseUrl = 'https://dataservice.accuweather.com/'
;

const httpService = async url => {
    const response = await fetch(baseUrl + url);
    if (response.status === 200)
        return await response.json();
    else
        return new Error("API, doesn't work! Error:" + response)
} ;

export const findTheCity = async freeText => httpService(
    `locations/v1/cities/autocomplete?q=${freeText}&apikey=${apikey}`
);

export const currentCondition = async code => httpService(
    `currentconditions/v1/${code}?apikey=${apikey}&language=en&details=false`
);

export const dailyForecasts = async code => httpService(
    `forecasts/v1/daily/5day/${code}?apikey=${apikey}&language=en`
);
