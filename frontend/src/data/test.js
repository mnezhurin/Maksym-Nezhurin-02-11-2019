// entered text - khar
export const res = [
    {
        "Version": 1,
        "Key": "308406",
        "Type": "City",
        "Rank": 20,
        "LocalizedName": "Khartoum",
        "Country": {
            "ID": "SD",
            "LocalizedName": "Sudan"
        },
        "AdministrativeArea": {
            "ID": "KH",
            "LocalizedName": "Khartoum"
        }
    },
    {
        "Version": 1,
        "Key": "323903",
        "Type": "City",
        "Rank": 21,
        "LocalizedName": "Kharkiv",
        "Country": {
            "ID": "UA",
            "LocalizedName": "Ukraine"
        },
        "AdministrativeArea": {
            "ID": "63",
            "LocalizedName": "Kharkiv"
        }
    },
    {
        "Version": 1,
        "Key": "2773473",
        "Type": "City",
        "Rank": 45,
        "LocalizedName": "Kharghar",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "MH",
            "LocalizedName": "Maharashtra"
        }
    },
    {
        "Version": 1,
        "Key": "189101",
        "Type": "City",
        "Rank": 45,
        "LocalizedName": "Khargone",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "MP",
            "LocalizedName": "Madhya Pradesh"
        }
    },
    {
        "Version": 1,
        "Key": "191582",
        "Type": "City",
        "Rank": 45,
        "LocalizedName": "Kharagpur",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "WB",
            "LocalizedName": "West Bengal"
        }
    },
    {
        "Version": 1,
        "Key": "2879567",
        "Type": "City",
        "Rank": 45,
        "LocalizedName": "Kharagpur Railway Settlement",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "WB",
            "LocalizedName": "West Bengal"
        }
    },
    {
        "Version": 1,
        "Key": "191530",
        "Type": "City",
        "Rank": 45,
        "LocalizedName": "Khardaha",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "WB",
            "LocalizedName": "West Bengal"
        }
    },
    {
        "Version": 1,
        "Key": "187303",
        "Type": "City",
        "Rank": 55,
        "LocalizedName": "Kharagpur",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "BR",
            "LocalizedName": "Bihar"
        }
    },
    {
        "Version": 1,
        "Key": "190121",
        "Type": "City",
        "Rank": 55,
        "LocalizedName": "Kharar",
        "Country": {
            "ID": "IN",
            "LocalizedName": "India"
        },
        "AdministrativeArea": {
            "ID": "PB",
            "LocalizedName": "Punjab"
        }
    },
    {
        "Version": 1,
        "Key": "257209",
        "Type": "City",
        "Rank": 55,
        "LocalizedName": "Kharan",
        "Country": {
            "ID": "PK",
            "LocalizedName": "Pakistan"
        },
        "AdministrativeArea": {
            "ID": "BA",
            "LocalizedName": "Balochistan"
        }
    }
];

export const firstRes = [ {
    "Version": 1,
    "Key": "215854",
    "Type": "City",
    "Rank": 31,
    "LocalizedName": "Tel Aviv",
    "Country": {
        "ID": "IL",
        "LocalizedName": "Israel"
    },
    "AdministrativeArea": {
        "ID": "TA",
        "LocalizedName": "Tel Aviv"
    }
}];
export const secRes = [
    {
        "LocalObservationDateTime": "2019-11-03T09:45:00+02:00",
        "EpochTime": 1572767100,
        "WeatherText": "Sunny",
        "WeatherIcon": 1,
        "HasPrecipitation": false,
        "PrecipitationType": null,
        "IsDayTime": true,
        "Temperature": {
            "Metric": {
                "Value": 23.3,
                "Unit": "C",
                "UnitType": 17
            },
            "Imperial": {
                "Value": 74,
                "Unit": "F",
                "UnitType": 18
            }
        },
        "MobileLink": "http://m.accuweather.com/en/il/tel-aviv/215854/current-weather/215854?lang=en-us",
        "Link": "http://www.accuweather.com/en/il/tel-aviv/215854/current-weather/215854?lang=en-us"
    }
];

export const responceDailyForecasts = {
    "Headline": {
        "EffectiveDate": "2019-11-06T07:00:00+02:00",
        "EffectiveEpochDate": 1573016400,
        "Severity": 5,
        "Text": "A thundershower Wednesday",
        "Category": "thunderstorm",
        "EndDate": "2019-11-06T19:00:00+02:00",
        "EndEpochDate": 1573059600,
        "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/extended-weather-forecast/47209?lang=en-us",
        "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?lang=en-us"
    },
    "DailyForecasts": [
        {
            "Date": "2019-11-03T07:00:00+02:00",
            "EpochDate": 1572757200,
            "Temperature": {
                "Minimum": {
                    "Value": 62,
                    "Unit": "F",
                    "UnitType": 18
                },
                "Maximum": {
                    "Value": 80,
                    "Unit": "F",
                    "UnitType": 18
                }
            },
            "Day": {
                "Icon": 6,
                "IconPhrase": "Mostly cloudy",
                "HasPrecipitation": false
            },
            "Night": {
                "Icon": 35,
                "IconPhrase": "Partly cloudy",
                "HasPrecipitation": false
            },
            "Sources": [
                "AccuWeather"
            ],
            "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=1&lang=en-us",
            "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=1&lang=en-us"
        },
        {
            "Date": "2019-11-04T07:00:00+02:00",
            "EpochDate": 1572843600,
            "Temperature": {
                "Minimum": {
                    "Value": 62,
                    "Unit": "F",
                    "UnitType": 18
                },
                "Maximum": {
                    "Value": 82,
                    "Unit": "F",
                    "UnitType": 18
                }
            },
            "Day": {
                "Icon": 4,
                "IconPhrase": "Intermittent clouds",
                "HasPrecipitation": false
            },
            "Night": {
                "Icon": 36,
                "IconPhrase": "Intermittent clouds",
                "HasPrecipitation": false
            },
            "Sources": [
                "AccuWeather"
            ],
            "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=2&lang=en-us",
            "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=2&lang=en-us"
        },
        {
            "Date": "2019-11-05T07:00:00+02:00",
            "EpochDate": 1572930000,
            "Temperature": {
                "Minimum": {
                    "Value": 63,
                    "Unit": "F",
                    "UnitType": 18
                },
                "Maximum": {
                    "Value": 81,
                    "Unit": "F",
                    "UnitType": 18
                }
            },
            "Day": {
                "Icon": 6,
                "IconPhrase": "Mostly cloudy",
                "HasPrecipitation": false
            },
            "Night": {
                "Icon": 35,
                "IconPhrase": "Partly cloudy",
                "HasPrecipitation": false
            },
            "Sources": [
                "AccuWeather"
            ],
            "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=3&lang=en-us",
            "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=3&lang=en-us"
        },
        {
            "Date": "2019-11-06T07:00:00+02:00",
            "EpochDate": 1573016400,
            "Temperature": {
                "Minimum": {
                    "Value": 64,
                    "Unit": "F",
                    "UnitType": 18
                },
                "Maximum": {
                    "Value": 79,
                    "Unit": "F",
                    "UnitType": 18
                }
            },
            "Day": {
                "Icon": 6,
                "IconPhrase": "Mostly cloudy",
                "HasPrecipitation": true,
                "PrecipitationType": "Rain",
                "PrecipitationIntensity": "Light"
            },
            "Night": {
                "Icon": 35,
                "IconPhrase": "Partly cloudy",
                "HasPrecipitation": false
            },
            "Sources": [
                "AccuWeather"
            ],
            "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=4&lang=en-us",
            "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=4&lang=en-us"
        },
        {
            "Date": "2019-11-07T07:00:00+02:00",
            "EpochDate": 1573102800,
            "Temperature": {
                "Minimum": {
                    "Value": 62,
                    "Unit": "F",
                    "UnitType": 18
                },
                "Maximum": {
                    "Value": 80,
                    "Unit": "F",
                    "UnitType": 18
                }
            },
            "Day": {
                "Icon": 6,
                "IconPhrase": "Mostly cloudy",
                "HasPrecipitation": true,
                "PrecipitationType": "Rain",
                "PrecipitationIntensity": "Light"
            },
            "Night": {
                "Icon": 35,
                "IconPhrase": "Partly cloudy",
                "HasPrecipitation": false
            },
            "Sources": [
                "AccuWeather"
            ],
            "MobileLink": "http://m.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=5&lang=en-us",
            "Link": "http://www.accuweather.com/en/bi/colline-bulemba/47209/daily-weather-forecast/47209?day=5&lang=en-us"
        }
    ]
};
