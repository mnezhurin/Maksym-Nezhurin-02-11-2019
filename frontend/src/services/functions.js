export const convertDegree = (value, type) => {
  if (type === 'Metric') {
      return parseInt(value, 10) * 9 / 5 + 32;
  } else if (type === 'Imperial'){
      return parseInt(value - 32, 10) * 5 / 9;
  }
};