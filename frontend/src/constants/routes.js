import WeatherPage from "../pages/Weather/Weather";
import FavoritesPage from "../pages/Favorites/Favorites";

export const baseRoutes = {
    index: {
        path: "/",
        label: "Main",
        component: WeatherPage
    },
    main: {
        path: "/favorites",
        label: "Favorites",
        component: FavoritesPage
    }
};