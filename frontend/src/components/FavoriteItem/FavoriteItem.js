import React, {useEffect, useState} from "react";
import WeatherBlock from "../WeatherItem/WeatherBlock";
import {connect} from "react-redux";
import "./FavoriteItem.css";
import {currentCondition} from "../../api";
import {callNotify} from "../../actions";

const FavoriteItem = ({code, city, showError}) => {
    const {LocalizedName} = city;
    const [weather, setWeather] = useState(null);
    const [temperatureType, setType] = useState("Metric");

    useEffect(() => {
        currentCondition(code).then(weather => {
            const {WeatherText, Temperature} = weather[0];
            setWeather({
                weatherText: WeatherText,
                temperature: Temperature[temperatureType]['Value']
            });
        }).catch(e => showError(`API for showing current condition error -  ${e.message}`))

    }, [code]);

    return (
        (!city || !weather) ?
            <div>Waiting...</div> :
            <WeatherBlock key={code} place={LocalizedName} tempType={temperatureType} temperature={weather.temperature} className="favoriteItem">
                <h3>{weather.weatherText}</h3>
            </WeatherBlock>
    )
};

const mapStateToProps = (state, {code}) => ({
    city:  state.cities.cities.find(item => parseInt(item['Key'], 10) === parseInt(code, 10))
});

const mapDispatchToProps = dispatch => ({
    showError: message => {
        dispatch(callNotify(message, 'error'));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteItem);

