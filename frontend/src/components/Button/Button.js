import React from "react";
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/icons/Favorite';

export const MyButton = ({text, onClick, isFavor}) => {
    const color = isFavor ? 'white' : 'black';
    return <Button
        variant="contained"
        color="primary"
        startIcon={<Icon style={{color: color}}/>}
        onClick={onClick}
    >
        {text}
    </Button>
};
