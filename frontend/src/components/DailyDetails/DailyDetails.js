import React from "react";
import "./DailyDetails.css";

export const DailyDetails = ({dayText, dayIcon, nightText, nightIcon}) => {
    return <div className="dailyDetails">
        <div className="dayTime">
            <span className="title">In the day:</span>
            <div>{dayText}</div>
            <div>
                <img src={dayIcon} alt="day weather"/>
            </div>

        </div>
        <div className="nightTime">
            <span className="title">In the night:</span>
            <div>{nightText}</div>

            <div>
                <img src={nightIcon} alt="night weather"/>
            </div>

        </div>
    </div>
};