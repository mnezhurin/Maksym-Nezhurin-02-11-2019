import React, {
    useState,
    useEffect,
    Fragment
} from "react";
import WeatherDaily from "./WeatherBlock";
import "./WeatherItem.css";
import {days, months} from "../../constants";
import {MyButton} from "../Button/Button";
import {connect} from "react-redux";
import {addFavorite, callNotify} from "../../actions";
import {currentCondition, dailyForecasts} from "../../api";
import {DailyDetails} from "../DailyDetails/DailyDetails";
import {convertDegree} from "../../services/functions";

const WeatherItem = ({code, addCityToFavorite, city, isFavor, showError}) => {

    const [daily, setDaily] = useState(null);
    const [weather, setWeather] = useState(null);
    const [temperatureType, setType] = useState("Metric");

    useEffect(() => {
        // Get ajax with code and then I have firstRes
        currentCondition(code).then(weather => {
            const {WeatherText, Temperature, WeatherIcon} = weather[0];
            setWeather({
                weatherIcon: WeatherIcon,
                weatherText: WeatherText,
                temperature: Temperature[temperatureType]
            });
        }).catch(e => showError(`API for showing current condition error -  ${e.message}`));

        dailyForecasts(code).then(items => {
            const daily = items['DailyForecasts'].map(item => {
                const {
                    Date: itemDate,
                    Temperature,
                    Day,
                    Night
                } = item;
                const {
                    Minimum,
                    Maximum
                } = Temperature;
                const date = new Date(itemDate),
                    day = days[date.getDay()],
                    month = months[date.getMonth()],
                    monthDate = date.getDate(),
                    preparedDate = `${day} (${monthDate} ${month})`
                ;
                const imgPath = `https://www.accuweather.com/images/weathericons/`;

                return {
                    day: preparedDate,
                    details: {
                        dayText: Day['IconPhrase'],
                        dayIcon: `${imgPath + Day['Icon']}.svg`,
                        nightText: Night['IconPhrase'],
                        nightIcon: `${imgPath + Night['Icon']}.svg`,
                    },
                    tempType: 'Metric',
                    temperature: {
                        min: convertDegree(parseInt(Minimum["Value"],10), 'Imperial').toFixed(1),
                        max: convertDegree(parseInt(Maximum["Value"],10), 'Imperial').toFixed(1)
                    }
                }
            });
            setDaily(daily);
        }, (e) => showError(`Daily weather error: ${e.message}`));


    }, [code]);

    const {Country, LocalizedName} = city;
    const name = LocalizedName;
    const country = Country.LocalizedName;

    return (
        <div className="weather-item">
            <div className="weather-item__top-part">

                <div className="city-details">
                    <div className="city-details__image">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFRUXFxgYFxUXFxYVGBYYFxYWFxcVFxgYHSogGBolGxgXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGzElICYtLS0tLS0tLS0uLS8tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAAABgQFAgMHAf/EAEMQAAEDAQUEBQcKBQUBAQAAAAEAAhEDBBIhMUEFBlFhEyJxgZEHFVKhsbLRFCMyQlNicpPB8DM0gsLhFiRzkqJjQ//EABsBAAEFAQEAAAAAAAAAAAAAAAABAwQFBgIH/8QAPREAAQMCAwMKBQQCAQMFAAAAAQACAwQRBSExEkFREyIyYXGBkbHB8AYUUqHRIzPh8TRCFXKCkhZDU2Ki/9oADAMBAAIRAxEAPwCuhbW6wqIRdCIRdC8hF0L2Et0IhF0LF5AzTUs7IhtPNk9DBJM7ZYLqVs2wur/QLGj0qj20x3Xjj3SoZxalAzd3WU9mC1r+iy44jMJhp7g2pwvB9FwOrXkjxiFHfjLP9Gk94H5UmLAzpLJs/wDaT+FqrblWpmdJzubS13qBlQ345PuiHirKL4eoj0pz/wCNvO6rK+yyww8PaeDhdPgQmT8QzjVg+6nN+E6Vwu2Un/xK1OsPB3qTjPiM/wC0fgf4TMnwcP8ASbxb/K1OsbuRU2LHqZ/Su3tH4VZP8K10ebLO7DY+Bt5rS6mRmCFaxTxyi8bgewqiqKWandsysLT1i39ryE7dR0Qi6EQkuhEIuhEIuhEIuhEJboQGT3LkuAtfelAJ0QG+pBcAQDvQASLhMe7m6NS0jpHu6OloYlzvwjhzPrUGqxBsJ2Wi58lYUtA6YbTjYfcpkduDZo/iVp4yz2XVBGKTcB771POFQ21Kvtj2JlmotosxDZk5FxJkuP74KFNI6V5ed6mwRthYGNUw103sp3aXnTo2Uba96dGyk2lxG6tasci6hCLqEIuoQt9hsFSs65SY57uAGXMnIDmU3JKyNu082TkUT5XbLBcq8s+7QFRtFzhVrEx0bD1GcTUqaxqG+Ko6nGudsQDPifwtNRfDt2ctVOs3gNT793VtvZsihZqNKk2mwvcS5zy0TDQAQJxAJI/64yqWsqJXgbbiT70WhwikhD3FjAAN35O9MlhstK3WOn0gEht28MC1zeqSOGWSRobLGLriR76WodscdN1tUlWmlXsNYtD3MOYc3APGhjI9hUMh8TtVdsdDVx3Iv6Jm2FvmHQy0ANOlQfR/qH1e3LsUmOpBycq2pwwt50WY4b011aLKjYc1r2nQgOB8VJIB1VU1zmG4Nilrau5VJ8miejd6JksP6t7vBR30zT0clYwYpIzKTMfdJW0tm1aDrtVhbwObXfhOqhvY5hsVdwzxzC7CohE5oY9zHbTTY8QupI2SNLXgEcDmFHq2QHLD2K9o8dkZzZxccRr/AD5rK4j8KxSAvpTsngeifUfcdSiOYRgQtPFMyVofGbhYWop5aeQxytLXDcfeY615dTqZRdQhF1CEQhKr3ZuwX9Ix7rppReJnNpGRGc4rFYt8VUzaeWGMkTAltraOB1vpYajfxC0VDgkxmY9wBjOd+II0tqrax7JoUi4i8+8C2DiADmBlyxWRr/ifEKxkbTZuyQ67bgkjQnXictFoqXAKeBzna7QIz4H3vWQ2XZwx1O45ocQScZwyxP7xXH/qXETVMqXOBLQQBawsdchx49QXX/BU3IuhaMjvvnlomWx28ZYBoADQMgBp7PBTaD4gD5nuqzsg2tw3367m/wBlxPQFjA2MLd8qkStTSytqIxKzQ6flVcoMbtk6rU60qWGJgvWt1qShi55RY/Kkuwk5RHypHJo5RcuhaG6zKIRdCIRdCZt2dz6lpipUmnR0P1n/AIRoPvHulV9XiDYuazN32HvgrOjw503Pfk37n3xVxt3alOytNksYDIwqPbnPC9mXcTpl2Zarq3vdmblbrCsLjY0OIy3Dj1lWW4WzAykazh1qmA5MB/U49wXNMyzdrilxSfbk5MaDz/hVHlCf/uGDhTHrc74BNVR5wUvCRaInr9FO8nVswq0ToQ9vf1Xexviu6V2rUxi0di2Qdivd5NjC00i3J7cWO4HUHkfhwT0se2FBpKkwSX3HVctqMLSWuEEEgg5gjAhVpFslqAQRcJh3Y3ldZyKdQl1LxNPmOLeXhzkQz7OR0VfW0Il57Ol5/wArotKoHAOaQQRIIxBByIKngrPEEGxWu2WRlVpZUaHNOh9o4Hmkc0OFiumSOjdtNNiuc7y7uOsxvtl1InA6t5O+P7MCaEszGi0NHXNnGy7J3n2fhUKYU9Y1KYdmpdHWyUr9pmm8bj/PWq/EsNhrotiQZ7jvB/HEKG5kYFbinqWTxiRmh+3UvLKyjlpJjDKMx4EcQsYT11FXoaguAzKUC+iut39ntqX+lpy0QQ43m46iRn++KxnxVi8tGIzSS2cbgtyOXGxvbPfv7locDoGVJc2aO4FrHMZ8OvsTRQo3onBowDRyXlssp2i4m7jmT1lbjmxAMZu+ymsaBkIUUknVNEk6r0oukUW0WeOs3A8NCnmSXycnmPvzXLFteWzw04LVYDiNRy0dIX2ZnwvxsD1lVeJ0sbWOl2bn3mtD7SvRQxZYvVxs/Yj3gOqEtByaPpd/BRZKkNNm5qZFSucLuyVj5hox9btvfsJn5l6kfKRrT/p1npv/APPwS/NO4Bc/Js4lcjurTrIIuoQnTc3dK/Fe0N6ubKZ+twc4cOA17M6murrXjj7z6BXeH4dtWllGW4epTJvbtj5NSusMVH4N+6NXd2nMhZ+aTYHWVrKCl5aTPojX8LnFloGpUawZvcGzzcYlV7RtGy0cjxGwu4DyXYbPRDGtY0QGgAdgEBWoFhZZBzi4kneue7//AM0P+NvvPUGp6a0GFfsHtPooO6ls6K1Uzo43D2OwHrjwXELrPCfro+UgcOGfguqKyWXSNv7seCLSwYGBUHPJr+/I9yh1Mf8AsFdYXU3/AEXd349fFJqiK5TNuhvD0DhSqH5pxwJ+oTr+E68M+KkQTbJsdFWYhR8oOUZ0h9/5XRQp6z6wq0muaWuAIIgg5EHQoIvklBLTcLl+82xTZqkCTTdJYeHFp5j2Kumi2D1LTUVUJ2Z9Ia/lU6ZUxabQ5upE8FbYRUuinDNzsu/cVnfiShZUUpk/2YCR2bx6jrC1XVsV5osqVBziGtEk5CQPamp52QMMkhsBqbE+QKciidI4MZqez1TpZabxTp06hl2ROeXPXD2LxHFJqeWsllphZhOW7vtuubmy9Nw2OWKmaJjdwGe/+1bAKlJuu0JEIQhaq50TjAnGDeoowf2+1PasTxzZ2Kx2Tsq9VFRz2vaMbuIIOgIOg/RenYdXUrqRsVNwF+PXfff2Fk5aOX5gvlz95JoTykIQhCELh0LXXWFTJuZu/wDKH9JUHzTDl6bs7vYMz3Diq+uq+TbsN1P2Cs8No+Wdtv6I+5/C6YqFadco3j2l8orvfPVHVZ+Ea95k96rJX7TiVqaODkYg3fqVM3Hs1+1tPoNc/wBV0e96l3Ttu9NYm/ZgI4kD19F0xWCza535Qh/uW86Q996g1XTHYtBhR/RPb6BLIMYjPRRlZkXyK6/sq19LRp1PSaCe2MR4yrVjtpoKyE0fJyOZwK3WqztqMcxwlrgQRyKUgEWK5a4tcHDULkm1LC6hVfSdm04HiDiD3hVb2bLrLWQSiWMPG9RFynU/7j7a6RvQPPWYOqT9ZnDtHsjgp1PJcbJVBiVLsO5Ruh16j/KbFJVWqHfem02OoSMWwWng68APaV0yn+YcI+KQ1poxywF7buK45XqvmHE92AUGWndA/ZeLFaCnrmVUYkjdceXUQvLJTl3IKdhcJkqA7c3P8Kpx6qbDSOZ/s/IDzPh5qxhay688WVKleMEhvMzA8E1PMYmF4aXEbha58U5HGJHBpcG9Z0CdaGBpda8Ii9xwiV4TWEullJbsnaJ2dLZ6dy9SpwBTgB21kM+PWrBV65QkQvHOhKBdKBdRiZTqeAstVT6TU43QrsdEpq2O8GmAGkRyMHmDqt5gkjXUrWtaRYcDY9d991R1IIkNyp6uFHQhCEIXGLLZXVHtpsHWcQ0dp1PLVap7wxpcdAsRGwyODG6nJdd2bYW0KTaTMmiO06k8ycVmJJDI8uO9bKGJsTAxugUHey29DZqhBguFxva7Ce4Se5R5nbLCVPoouUnaN2vguWKtWpTl5OKXWrP4BjR3lxPsCl0ozJVPi7smN7SnlTFSJB8orPnaR4sI8Hf5UKq1CvcIPMcOtKSiq2XQPJ7bL1F9I503SPwvx94OU6mddtuCoMVj2ZQ/iPJNakqrSZ5R7I0U2V46zTcIGbmmSPAg+JTMtO6QXaNM+5T6GvZTnYkNg45dv8/hc2qbQccgB61CDOKu3THctuybVUbVFRryHNxB4Hsy4q1wqASSm4yAP3yWc+Iqt0dMADzi4W7syun7J3zovaBW+bfrgS08wRl2H1qbNh8jTzMx91R0+KxPH6mR+ypd8d4mV2ijRksBlziCLxGQAOMa+Cl0VI6M7b9dwUHEa9sw5OPTeUqOpg5iVPfGx4s4X7VWxTSRHajcQeo2Q2mBkIQxjWCzRYdSJZXyu2nuJPWbr26u00i6kJNskoTLsysx1MMp3ppx9IQdccCRxXkfxHQVcFUaipDbSHLZNxlbLMA3AtuzXoWBVlPJCIYr80WNxY578rhXFO0AifUss6Mg2Vo6Mg2Q6twQG8UoZxWoldLsCy8JQlXljouqP6onQfqVKhp5JniKMXJSTPEbM042abovCD6u5ej0+3yYDxYgbtO5UD7XyW1PrlCEIQhc/wBwbFerOqkYU2wPxOkewHxVziMlmBvH0WcwiHalMh3DzXQFTLRpK8o9owo0+bnHugD2lRKo6BXGEMzc/sHvwSSoau09+TkfN1T98e7/AJU2l0KosX6bez1TgpSqUkeUoACg8mBLx43CPYVFqm3AKt8JeAXgngkCpb2jKT6gogYVbOmaNFL2BvS+yVhULb1M9V7RnGcgn6w9eI5qVTssSVVYlLtNa08ffouw7P2hSrsD6Tw4HhmORGYKmvY5hs4KjjmZILtN0n7+7Sa8sosIN0lzyNHRAb2wTPaFaYdEW3ed+iosXqGvIibuzP4SRVsDXGcR2LqfDIZHbQy7Pwu6T4gqoGbBs4Djr4hbqFnDBAUqnp2QN2WKvra6ask25T2DcOxZ3VIuoaLqS6EXUXQi6luhF1F0L26i6FKsdtdSm6BJzJkzGQwOCpsUwWHEiOXcbC9gLCxO/MG5+3iVYUWJSUYPJAXOt7m9t2uX8phBBMtIDoktkexeSz0ksVy5p2LkB1jY9h0zXosFXHJZpIva9r5hZdNGYIUTYvoVJ2L6FHTjQEpOTO9Gwd6ybRc/QxwGJTjGE5MBJ6hdIXtZvTFsrZwaGVA4zGWEQdFrsIwpsbWVIedoi+61ju4/dVNTUFxLCMlbLRqEhCEIQhCEtbh0YoOdq6ofUAPip2IOvIBwCqsHZaEniUyqCrVc+8oh+fpj/wCf9zlBqukFf4T+07t9Eo1K7W5kKOASrIvaNSm/ycbXpzUpGQXEFpOAJAxb25KwpYX8m524FZzF6qPl2R3zINl0BPKCkPyhVm1Hso5hoJd2uiO8AT3qxpKdskTtsZH0VLX1z4J2GI5tzPfuKQqmynzgQRzw8VDkwmUO5hBHgrmH4mpnMvICHcBmO4/lZ22wXaBGZBvE+31KaaIRUxbqdSffUqf/AJV1VXB5ybbZA9772UjY1a9SEHEdU92XqhS6R+3EL7slXYjGYqh1t+fj/KmXVKVetFW102mC4T3n2JQCUqw84UvS9R+CXZKEecKXpeo/BGyUI84UvS9R+CNkoR5wpel6j8EbJQvW26kcL47wR7UmyUKTCRCLqEiISJVts1UscHjMT6wRiodfRMrKc07sgbdwBBy8MlIpKl1PMJhqL/cHVTrNtV4Dr3WMdXIYzlhp8FnK/wCEqWV8fIDYF7Osb5WJvnfO4t333K5pcfnY1/KnaNubuz7t2/uUqw7baINVl7HENMYQcceB0kZph3wdCKoFjiY7E2P1XAtcbsye6xUiP4kfyX6jedfdwzzseBy1V7ZN4rHTvFrn4x1bjp7Jy9am0eAmkc7kxkbb/eS7kxuCQDaJv2H+lfbMtjK1MVKeR01B1BjVOviMTiwhSIZ2zMD27/dlKXKdQhCEIQhCX9yHj5PdnEPdI7YIUyuB5W/UqvCXD5e3AlMChq0XOPKPT6WsAw4sYAccDJJg9xCkfI8tDtjpXy7FGZjPylTyTugQL9R4+CQzZ3zFx08IKreQl2tnZN1oxW05ZygkFuN1fbPsvRsAOeZ7VpKODkYtk6nMrz/Fq4VdSZG9EZDs4pgo7yWprbvSTwJALh3698oNHCTeyabiVS1uzteIzVTUJcS5xJJMknEkqSLAWChOcXG5OaxupbpF46mCCDkcEHMWKVri0gjcqXYRu1H0j+y0wfV7FW0RLHujPuyvMUaJIWTD3dTdrWm42B9J2XIalWzBdUSX08hCEIQhCEIQhCEK42LaZ+bJ5t/UJqQb0K2upq6EXUXQi6i6EXUXQi6i6EXUXQpFjtdSkZpvc06xke0ZFcPjY8WcLp2KeSI3YbKVadt2l4h1Z0cob7oCbbTRNNw1PSV1Q8WLz3ZeSs9nb21WANqtFQelN13fofUo8lC12bDZS4MWkYLSC/Xof5Vn/rOjH8OrP9HtvJj5CTiPv+FL/wCZit0T9vytX+tGfYu/7Bdf8e76gk/5ln0HxH5S3s63VKD77D2g5OHAqdLE2QWcqaCofA7aZ/aZH74i7hSN/mRdntzPgoIoDfpZK3OMjZybn9koWy2AuLqjxecZMkDNWTGWFmjJUb3ue4udqVrFoZ6bf+wXViubKPado02jBwcdAMfE6Ll92tLraJ2GPlHhvFTHQJJMAapU0oL9rUgYknsC7Ebkqx88UvveCXk3IR54pfe8Ecm5Cqq9pAr9KzLUHCTEEeCifJP5blAbBWLaxnyvIuFzu9FHtNdz3Fzs/ZyCsQ0AWCrVqSoQhCEIQhCEIQhCyY8gggwRkUEXQrqz7bbHXaQeIxHhomTEdyFu88UvveH+UnJuSo88UvveCOTcheja9LiR3JOTchTaT2uEtII4hcHJIs4SXQiEXQiEXQiEXQiEXQi6luhboXF0IhF0KPWsFN+LmAk65HxC6D3DQoVXatga03dzv0KdbNxSqIdj1GguddAbjnJOKbqZQYiApFL+81S95axBawZfSPPGB7CnIRqVGVEn0IQhCEIQhCEIQhCEtlPs+xLTUxZZ6rhxDHR4wmnTxN1cPFPNppnaNPgtlXd+1tEus1Yf0OPsCQVMJ0cPFdOpJ26sPgq57CDBBBGYOBHcngQcwo5aQbFYoSIQhCEIQhCEIQhWWwaxFUN0dII7BIP74puUc26VM8KLdIiEXQiEIRCLoRCLoRCLoWy6uUIuoQi6hC9ZTJMASTkEhcGi5XTWlzg1ouSruy7EpgfPQ6cLv1ezmVTVOIF3NZkPNamhwUR2fLm7huH5Sbv/AGRtOuwNmDTmDjHWcrnDJ3TRku4qnxSkZTShrNCLpYViqxCEIQhCEIQhNm625NW1AVKhNKicjHWePug5DmfWoFTXti5rcyrOkw58w2nZDzT3ZrLYLDgxjb4zMX6ne45dmCzNbjLGm0j7ngP49VpqTC7D9NneV7U3qb9Wm49pA9kqodjrNzD3kD8qzbhjt7l43eoa0j3OB/RIMdbvYfH+Epws7nfZe17bYrULtemP625djx9HtkKfTY5DfJxaev3ZQ6jCnOHOaHD33pU3j8n5a01bGS9ufRky6PuO+t2HHmVqKbEw6wk8dyzNXhJbd0XgkNwjA4HgrbVUpFl4hIhCEIQhCFO2L/HZ2n3SuJeiUJvuqGhF1CEXUIRdQhF1CF5dQhbYXCEQhCIQhXOydnuab7hp1cRrr4e0qor6kPbsM71p8GoHRv5aUbub371ZMlx48sMOaqRclaU2ASB5SAenpz9kPfetPgtxE6/H0CyOPkcs23D1KUlcKhQhCEIQhCdPJ/usLQ75RWE0mmGtP/6OHHi0es4aFVtfV8mNhmvkrfDaISHlH6DTrTbtzbRJNKiYGRcMycrrY0Xn+I4m5zjDCeokak8AtzSUTWt5STuH5Rs7dokXqpLfujPvOiKXBS4bUxt1DXvP4RPiIBtGL9atKOy7KHXA1rnRJBN49pBKsWUNEHbAaCeBNyojqmpI2iTbwW5+xrOf/wAmjsw9iddh1KR0B5LgVcw/2KpLTsuyuN2lWa12gLrzSeE/5VRLQ0TzswyAO4XuPfep8dTUNF5GXHZZRLNaq1kfdcDGrdCOLT++aixTVGHybDhlw3HrHvtTz44qtm03Xj+VF353dZaKXy2zjrAS9oH02jN0emNeIHJb7CsQa9oF7tOnV1LG4th5BLwOcNevrXNFfrNIQhCEIQhT9h/x2dp90puXoFKnKFBSIhCEQhCIQhEIQi6hCzhIhEIQiEITVZXdRv4R7Fm5T+o7tPmvQqcfos7B5LOztaCY7CmmADRPvLiBdc+8pv8AMU/+Ie+9abB/2ndvoFksd/eb2eqT1bKjQhCEIUjZ9kdWqspN+k9waOUmJ7s1xI8MaXHcnIozI8NG9dm2kW2SytpUsIAY3jEYu7c8eJWExescyIuvznZfn7Le4dTNLg3c1Rd1dnD+M4cmd2bv08VW4NSC3Lu7B6n0U7Eag35Jvf8AhbLVbatpeaVA3WD6T8p7+HZmu5qmaskMNObNGrvfs9i4jhjp2cpLmdwVhszYtOibwkuiC4njngMFOpMNipzttzdx/hRp6uSYWOnBat6KhFEAGA5wa48BjPsTeMPcIAAbAkAnqXdA0GW53Akdq9tOxqHREXGiGk39RAzJ1Sy4dSiEgNAsNd/bdJHVzcoDcnPT0sotmsptNjbe+mJuE54EgCeByUaKA1lA3a6QvY9n50Tz5BT1R2dN6i7p2sh7qRydiBwIzHh7FGwWoLXmE78x2jX31J7EogWiQewueb57IFltT2NEMd12cmu07jI7gvSqOblYgTroV53XQcjMQNDmFRKUoSEIQhCsNgj/AHFPtPulNzdApQCdE6QoCREIQiEIRCEIhCEQhCzhIhEIQiEXQr+g7qiOA9iy8p/Ud2nzXo9MP0Gf9I8lvpVMjezwjKSuWnfdOObuskLymfzFP/iHvvWnwb9p3b6BZDHf3m9nqk9W6o0IQhCE2+TKyh9tDiP4bHO7zDR7xUDEnWhtxKs8KZtT34BOO99WarW+i2e9x/wF5xjkhMzWDcPP+l6BhjLRl3E+X9q7rWN4s3RUyA66GycPxd5x8VcyU8gpORiyNrflVzZWmflH6XutuyLCKNJrdc3Hi45/DuTlFTCniDN+/tXNRMZZC7wU1S0wtNrszajSx4kH9yE1NCyZhY8ZFdxyOjcHN1VT/p8xcNeoaY+py4Z/oqz/AIkkbBlcW8Pf4Uz54DnBg2uKuaNIMaGtEACAFasY1jQ1osAoLnFxudUnVR0dtw+1Hg+J94rKP/RxHL6h9/7V439Skz4eSg+VqzC5Qq6hzmHvAcPdPivQ8LdznN71iMYZzWu7lzZXKoEIQhCFYbv/AMxT7T7pTcvQKcifsODk3stbbxa4gEKtmla081XNPhIqRyt9lpUl8RLZdAmAJPdxUZ9YyNhc/IBdTYDKHtERvc2zyt1nqUOzbRpvmHQRmHQCNO/uSQV8EwyNuo5e+5RazBaylPOZtDcW3I/I7wFMhTFVLyEIXqEISIQhCEIVxQOA7B7FlZf3Hdp816XSj9Bn/SPJSWOPAHHwHFICV2QEieUk/P0/+L+961GDftO7fQLHY/8AvN7PVKKuFRIQhCEJ38k7h8pqjXov72KsxT9tvarjBz+o7sTFvRhaJPotPdJ+C84xfKrueA9VvaDOC3WVd7w22pSY19Mx1oOAOBBOquMTqZIImyR8c+yyr6KFkjy1/BWrHSARrirNpuLhRCLZLJKkQhCEIVO23PNrNIHqBskQM4GveFVCqkdXGEHmgZ9vsqaYWCm5Q6kqk2j1rbA+0YPC7Kp6vn4hYfU0eSsIObSX6j6rV5WD/tqQ/wDsPcet/hf7p7PVYvF/2R2+i5YrxZ1CEIQhWGwP5in2n3Sm5ugUq17fe+jaXiZvfOCMSGknMaQQVRy2ac1tcKmElK2w0y8Ftse2K4YX08A0SC51wGHNBAJ/EJUWoc0MINuxW9PHykgbnbqBTMwUbeS6mQ2owEOIEBzp4iQRHAnPWFnZIjHmMx5KftPp7B2h9+75ryyU61F4pOLKmGTXAubjiYJmANOWCsqCvkYQx4JaSAOq/os3jNDRVO1JG4NkAJIy51uob+vxVktKsOhCFIsLA6o0OEgz7Co9U8siLm6/yp2GxMlqWseLg38ipm0LBl0bOMweyMyotLV68q733BWmJ4WQW/LM43sey2pUivYGXDdZ1owxOfimY6t/Kc52Sl1GFxfLnk2c62Xb4qRYrOA0Xm4qE5oLies+at4i5sbW8GgfZacDIKjqZnqkTyiE9PTn7P8AvetTgn7Lu30Cx3xDbl224epSorlUCEIQhCY/J/bhSttOcA+aZ/q+j/6DVDr49uE9Wan4bIGTi+/JdD3ws30Kg/Ce/Efr4rzzHYbhso7D6eq3+GSZlnepFjItVluT1gLvY5v0T3iPWpEFq2i2D0hl3jQpqW9NU7W70Oq93d2hI6CphUZgAdQNO0JcLq7t5CTJzcu0fwkrYLHlWdEq8VwoCEIUTaVvbRYXO7hq48Ao1VVMp49t3cOJT0ELpXbLVU7FZ0bKlqq5uk92frOXcqvD28lG+rm1Ofd/O7uUyqO25sEeg8/4Vdu/SNW0dIdCXntMwPE+pQMMY6er5R267j36KVWuEUGwOxU3lZtoL6NEH6IL3f1QG+oHxXomFsyc/uWFxiTNrO9c/VsqRCEIQhWGwP5in2n3Sm5ugUJq2rYKlRoNJwa4HJ0wRhqMll8Tws1diDpu3LT4HXNgjLXDK+vgqO17NrF/WovJg9YS5pkAOY66ZuGBIwVDHRVlKbmMkaEDO/XldaqHEKe9w4D7ee9QtiWOsbRSllak3MubQdTu3iL1NxeMQBMEl0aY5WbaeU6NPhZTqrEKbknc4E9br94sfG1k3bS2HSqVBUb1DeJdE9aYnGcDhmOOPKZDQSHp5eaw9XHTyO2m67+BUlWyzKEJFI2efnG9/ulRa39h3d5hWWEf5sff5FXAdwnDGOM6LO3W8IUhrk4CmyFEtO1G0nBjrxMAyI1JHFRZa2OB4Y6906ymdK0uGixOZ712dV2NAkbyhfxqf/EPfctVgf7Lu30CxvxF++3s9SlVXSz6EIQhCypvLSCDBBBBGhGIKCLixSgkG4XbdhbRp2+yBxzIu1B6LxE/oR2hZOvoxzon6FbKhq9trZW6hUlKpUsdYgiRqNHt0IWNjdLh9QQe/rHEe+paVzWVcVx/RV3aLHStYFWm668fWGYIyDhmDzVxLTwV7RLE6zuP5Cr2SyUp2Hi44fhaW2i20sHUxVHEYn1Y+ITTZsRgGy5m31+/wuzHSSZtdsr07Ttb8GWe6eLp/uhKa2vfkyG3b/NkfL0rc3SX99V0UtkGemtdQOjQnqjtPDkEkeHm/L1jr23bvfVoh1ULcnTtt5qt2ztQ13CnTBuTgNXHQxw5KBX1zqpwiiHN8z7/ACpdLTCAbb9fJXliossdBz6jgIF+o7sGQ48Bx71oMNoeRYGDpHXt/AVRW1QeS86BcZ23tJ1prvrO+ucB6LRg1vcAFtYIhEwMCw9RMZZC871BTqYQhCEIVhsD+Yp9p90puboFC6dsWix7KrS0FwbLeOR/UDxVNM9zHNIOSucK2XNe0jrUupsgOLCx8B4kAidAc+yVy2qc0EOGitDACQWnVbLJsZoLzUcXXTkMB9Fru3VcyVTjbZFrpWU4F9pFaw0hd6sRSc8wToGxOPMpGzSZ577eaSRjA25G4lLSmrJIQhb7GeuO/wBhUSu/Yd3eYVng/wDmx9/kVbsdxWdBW9IW1rl2CuCFyqnZ6tS8+9UgYk4mJeADn1getlqO9aqaenje1jmtudL2zsLndlbLXjrfJYiOOoe0uDnW6r5XNuOd89N+uSd9zKNR1maXSTeeJcccHERxVJicYNSSy1rDyWlwiUilG2Te580veUandr0wfsv73q3wRuzC7t9AqL4gdtTtPV6lKauVQoQhCEIQhXO6+8D7FVvt6zHYVGekOI4OGn+VHqaYTttv3KXSVbqd9929dcpvs9uoh7HBzTkRg5h1BGh5FZOuoGyDk5hnu/IWvpKz/wByI5e9VRWjY9ooOvU5cPSZn3tz9qzMuHVVM7aiuRxGveP7V4yrhmGy/LqP5XtPeSu3BwafxAg+qErcZqI8ngHtFj77khw+F2bb92ayfvPVP0WsHcT+q6djkx6IA+/qkGGxjUlaRZbVaSC68Rxd1WjsHwCZEFZWm7r268h77l2ZKemFhr1ZlMFg2bSsrTUe4SBLqjoAaNYnILQUGGsp9M3Hf+FVVVYZBd2TR7zXN9+N7PlR6KlIotMzkahGRI0aNB39mtoqPkuc/peSyNfXctzGdHzSkrBVaEIQhCEIVlu5TLrTTAEmTh/S5M1Dg2MkruON0jtlouV0bZlZ1CqHPa4NODpByOvPGFUSbMrLNOam0rn0swMjSBobgq3p26k1t0PHUdeZniwzLe0AkeHFMmJ5N7a69vv1V+JGAa6aLy0bVpdcAk3yBIByutDjjyBStp5MiRp/KHTszHFRbftNjm1C2ZfDGiIhgzPeSfUu2QOaQDuz71Br6loiIGpyHYqNSlnkIQrOybMeCCSBGmZVVU1bJIyxu/8AK02HYVNBM2Z5GV8t+hCsega3Eyf3yVXsAZlabbcdFJbTA0TgACZLiQuPNslqbIFCp+U4/WB9HHLWVrnfLvNy4ePVbu13WzWIa2pYC0MP/j19mfeui7i0XMsbGvaWmX4OBafpnQqixJzXVBLTfTyWkwtrm0wDhY5+aV/Kd/MU/wDiHvvVng/7Tu30CqMd/eb2eqTlbKjQhCEIQhCEIU7ZO1q1mffovLTqMw4cHDIpqWFkos4J6Gd8LtphT9sjyk0yALTTLD6bOs3tunEd0qqlwxwzYbq6hxdhykFuxMVLeiwVBPT0ux/VPg8BQX0Mn+zL911YMr4SLh/osnbw7PZj09D+ktJ8G4rltC++Uf2XTq6K2cg8VTbT8o1mYCKLX1XaYXG95dj6lNjw2V3SyUKXFomjmZ/ZIO395bRaz846GaU24NHM+keZVrBSxw9HXiqWprJJ+kcuCplIURCEIQhCEIQhXe5X87R7Xe45Q8Q/xn+94Vhhf+Uz3uK6vbLQKbS53hxOgWYhidK8NathNI2Nhc5KNp2jUc+9caB6InxnitLDTMYzZ2iVn5J3PfcADqWJtztGDvM/ou+RA1K4Mh4LZZL7zBiTlp3Jmo2Y27W5RX0kkz+ac+tb6lme3Np9o8QorJ439FwUaWiqIumwj7/cLUnlFuEz0wJMZ6rLiy9KN7LZTfIkLoG65cLGyyL4jAmeGnaluubL2+Ju6xKW4vZFsrrJKkS5vpu8bUxrqcdKyYBwvtObZ0Oo7+Mixw+rEDiHdE/brVVidCahgc3pD79S5zV2LaWmDZ6s/gcfWBBWhFTC4XDh4rMOo52mxYfArDzVaPsKv5b/AIJfmIvqHiFz8rN9B8CjzVaPsKv5b/gj5iL6h4hHys30HwKPNVo+wq/lv+CPmIvqHiEfKzfQfAo81Wj7Cr+W/wCCPmIvqHiEfKzfQfAo81Wj7Cr+W/4I+Yi+oeIR8rN9B8CjzVaPsKv5b/gj5iL6h4hHys30HwKPNVo+wq/lv+COXi+oeIR8tP8AQfAo81Wj7Cr+W/4I5eL6h4hL8tP9B8CjzVaPsKv5b/gj5iL6h4hJ8rN9B8CjzVaPsKv5b/gj5iL6h4hHys30HwKPNVo+wq/lv+CPmIvqHiEfKzfQfAo81Wj7Cr+W/wCCPmIvqHiEfKzfQfAo81Wj7Cr+W/4I+Yi+oeIR8rN9B8CjzVaPsKv5b/gj5iL6h4hHys30HwK9GyLQcqFX8t/wSGoiH+w8Ql+Vn+g+BTtuRuw+k75RXbddBDGGJE5uMZGMAOZVNiNc2QcnGct59FfYXhzo3crKLHcPVOVak1whwkKpZI6M7TTmrx7GvFnKvfsanoXDw+CmNxKUagFRDh8Z0utY2PT9I+r4JTiknAIGGx7yVIo2NjMQMeJxUSasllFnHJSoaWOM3AzWT3cIUMngpgA3ryEXK52AsuIBg4Yx+5S9iD1ra0meUZroXuuLCyzc2eOc4YJSLpL2WxdBcoSoXqEiEIQhCEIWL6oBAJAJMCTmeA4pbJQ0nQLJIkQhCEIQhCEIQhCEIXhci4CF6hCEIQhCEIXiEqCkQtZYJJ1K5tnddXystbBEyBJOn7zXIyXZKxe6EhNkoC08IwC47E52oQhLlq39sjTDRUqc2tAH/sg+pWzMKncM7D31Klkxmnaebc++tMOy7c2rSZUaCGuaCAYkA6GFTT1LIJXQu/1NlYxP5ZgkG9TmuBTsczJOiV0Qs08uV6lSIQheoSIQhRNpUHPZ1ahplvWDgYGAODuX75LtpsU7C8Ndzm3Byt+EqN2w91WlUq9YMnBoAJzkwczIHgn9gAEBXJpWCNzI8r8e7+fFN1htYqtvBrm4xDhB/wAhR3NsqWWMxu2Sb9ikLlNoQhCEIQhaLVbGU4vuichiSewBMzVEcIu8pyOJ8nRCzoV2vEtII/eB4LuOVkjdphuFy9jmGzglbadRxqPvTgSOwaNbzI1WbrJHumdt7irina0RjZ99ql7P242mRTrOgaPMwPuud+pU+hrT0JD2H0KYnoi7nxju9QEwtcCJBkHIjVXCrdNV6hIhCVeIKF4VylWBKRdBYOK5c4AXK6Ch2i1U2HrPidMdFHFRESbOBTrGOfoFppWtjoDajSfWe5AkadCuzG4ahSV2uVwh1QDVbkkLzoMJ3Lr25FUOsVEjg4eD3Bec4221dJ2jyC2mG/4rPe9Xiqw4g3CnrfSqTgVdUdZynMfr5ppzbLcFYhNoQheoSIQhR7fYxVbccXASD1TExocMl011jdORSmN20Puq9uxRTr06lIBrQCHDGciARxOOPYF3t3aQVJNWXxOZIbk6fb8K4TShIQhCEIQhCEJc2/RcKl7RwAHHD6vLj3qgxONwl2zoR7CtKJ7SzZ3j3dQrLtF1A3mtLwcHNBgd05lMUdSYH33b1Ilp2zCxNjxVjbKjLRSfUpgtrMYTccIcMJAI1mMCFazwx1LNtvSA99vaocW1TyBjzzSdd3viFS0nS1pyloPIAjTiSqFzS02VkdSOtStl7UNB/RkTTcCbs4sIzu9sgxxVhS1zom2dmPuPfBR6il5Zu2OkPv74pspVA4BwMgq9Y9r2hzdFTlpabFZLpIvEiVYkpEoWVGkXHlqV01hcuXvDQtVWyvvRE/BZ6qoK2aYtOY3HQf2kEzbXKTtrtqCq7pRddwzAGkHh/ldfLGD9MhXdK5hj5mnqoUhLZSFlePEouksFnsbyeWZjQbRNZ+oksYDyDYJ7SceAWqmxORx5mQ+6yEGFRNH6mZ+ybbBYKdFgp0mhjBMAThJk58yqiphbUOL5Okd6sY2NjbssFgtG19qUbK2/WqBgOWZLjwAGJKrm4ZUPk2I236/yklqGRN2nmyTNq+UBxwszLv334ntDRgO+exaKi+GmtIdO6/U38/i3aqGpxsnmxDvP4T1u9tMWmz062rh1hwcMHDskIqIuSkLFaUs3LRB6skypC9CEIQkQhCEIWNV0NJ4AnDPJdNF3AISFsi31nVaYbVe5znNLhecRGBfeBMDDWIzAiFqKqCFsbi5oAANsh3W9343UhzQBon9ZVR0IQhCFrtNBtRpa4SD+5C4libI0sdou2Pcxwc1JVOk6nNN5N5huk6n0S0cxBlZiojMchaVfbbZBtt0Of5v2IfbHUXU6o+o6CNLjvpNJ1OvIhO0tQ+Nw4BJyLZWuYd4v3jf6KdtCxEtNWzkFokupOwLCcSW8OMHDEkKXURsnj5WM5DUbx7/pRoJg13Jza/UN/b+fFU9MvNQFzLga063oJ17VWnZDSAVYENDCAb3Ka93Hm65ugIIGuPHwnvVxhTiY3N3A+apq4DbBVurQqEsSVylWJRqutFB3h3ss9gbccb9WJFJsTjq45MHbjwBVlT0r5BlpxVfJIL3KpdjeUdj2OdVoOD7xgMILSNBLiDIEA4c+SKmIQuAvdRJa1keTtUpbd2m+01nVX4TAa0ZNaMhz7eJVLNcvuVtsCqIJqQGH/uB1B96dXXdV6aVwvZKLBFguthSFkVsBSrkqh39sLathrTmwdI08CzEx2tvDvU2ikLJhbfkoNfEHwuvuzXHrOeqtQ3RY6QWcuo+SusTQqsOTakj+po+HrVHirbSA9S0ODOJicOtOQtLC65fbe9G8L3hmq/kn7O1sm3G2SuFtXCF6hIhCEIQhCFi1gBJAAJzMZrouJyJQslyhCEIQhCEKq29swVW3myKjci3BxGrZ171DroDJHdouR9+pS6SoMTrHQ8dO1KVSwlwh1R0A8ongOJWfEltyvGygZhozV9u/PSxpdN4ewu5z+qm4ZtcsbaWz9FWVtuT78lLtOwgXSxwaM7pEgHjzUqXCw512Gw4JmOtIFnC6sLDY20m3RjqScyeKnU8DYGbLVGlldI65W8p5cLAlIulTb17VfZbM+tTi+LobOQLnBt6NYmY5KRSRiSYNKaqHbMZsuNVqrnuL3uLnOMucTJJOZJ1K0wAAsFU6q5sDIpt54+Kz9Y/bmd1ZKmqXbUpW9wlQ3tDhZSMOxCWhmEsfeOI4fjgtJCguBBsV6vSVUVVC2WI3B+3UesLxIpC621PrJFbGrpclLnlE2h0ViqDWpFMYYC99Inh1QR2kKdQM25h1ZqvxGTYhPXkuSUBDQtO3RY+Q3cuoeTGzO+S1nA3S95DTnF1gF7nifUqTEnjlmg5218VosGbaIu4lY0dh1ulFMtukEHpReIwvOvh0QTLspmQAVZur4eSLwbj6cuoWtf76K9Lxa6elmEyvUJEIQhCEIQhCEIQhCEIQhCEIQo1osFN5lzRPESD4hR5aWGU3e3Pw8k6yZ7BZpWyz2ZlMQxoaPb2nMpyKFkQ2WCwXL3uebuN1sTi5XiRKsSUiUKv2vtIUGg3S8uMBowyBJ9nrClUlKahxF7W3roC6od5bS21WCuWAgsguaYkFjmvOWBwByT7ad1JUsD9DoeN8k3UM/TK5Wr5U6YaYwHYPYsvIbuJ6z5qhcbuKyXC5Xj2ym5GbQV5gWLmgms8/pu6XV/8AYevV2LVdPBQ7FelfNwf/ACN8VZbG8ozmNDbTSLyPrsgE/iaYE8wR2LQzYUCbxm3UV53BjBAtIL9YT7sfaLbRRZWYHBr5IDonAluME8Fnayo+WlMVrkeCuYZRMwPG9S6jQ4EOAIOBBxBHAhU75nudtk5+SdLQRYpT21uNRqy6h80/0c6Z7vq92HJX+H/EE7CI5Rtj/wDX89/iqaqweJ/Oj5p+yatibPbZ6FOi36oxPFxxce8kqXPKZZC871NghEMYYNynptOr1KkQhC9QkQhCEIQhCEIQhCEIQhCEIQhC8QlQgoWJK5SrAlIulV7a2WK4b1i0tnS8MY044DHtGqmUdaaYnm3vbq9/0u25LXszZgpNeHEOv5wIbdAgNAnLE+KSurfmHNLRayUi4suU7w7JdZazqZm7mx3pMOXeMj2K8pKgTRh2/eqWaIxvsp1ndLWnkFRTN2ZHDrKz0rdl5CzTSbQhCEJLBKtr0Wreuo9F2Lcb+Qs/4T77l5zjP+dL2+gW0oP8dqvVWKYsqeamUH74XLtFICvRoE0Vkuki9SpEIQvUJEIQhCEIQhCEIQhCEIQhCEFCF4hKvCkSrEpEBYuSLpYOXJXYWpy5K6SN5UPoUPxv90K2wjpu7AoNd0Wpa2b/AA29/tK4rf33d3kFlav90qSoqjoQheIQv//Z" alt="logo"/>
                    </div>
                    <div className="city-details__description">
                        <div className="city-details__name">
                            {
                                city ? `${name} (${country})` : "Waiting"
                            }
                        </div>
                        <div className="city-details__value">
                            {
                                weather ?
                                    (
                                        `${weather.temperature.Value} ${temperatureType === "Metric" ? "°C" : "°F"}`
                                    )
                                    : "waiting"
                            }
                        </div>
                    </div>
                    <div className="city-details__button">
                        <MyButton text="Add to favour" isFavor={isFavor} onClick={() => addCityToFavorite(code)}/>
                    </div>
                </div>

                <div className="weather-item__description">
                    {
                        weather && <Fragment>
                            <img src={`https://www.accuweather.com/images/weathericons/${weather.weatherIcon}.svg`}
                                 alt="Logo"
                                 className="weather-icon"/>
                            <span>{weather.weatherText}</span>
                        </Fragment>
                    }
                </div>

            </div>
            <div className="weather-item__bottom-part">
                <div className="weatherDailyWrapper">

                    {
                        daily && daily.map((day, index) => <WeatherDaily key={index} {...day}  className="dailyItem" >
                            <DailyDetails {...day.details}/>
                        </WeatherDaily>)
                    }

                </div>
            </div>
        </div>
    )
};

const mapStateToProps = state => ({
    code: state.codes.chooseCity,
    isFavor: state.codes.favorites.indexOf(state.codes.chooseCity) !== -1,
    city: state.cities.cities.find(item => parseInt(item['Key'], 10) === parseInt(state.codes.chooseCity, 10))
});

const mapDispatchToProps = dispatch => ({
    addCityToFavorite: (code) => {
        dispatch(addFavorite(code))
    },
    showError: message => {
        dispatch(callNotify(message, 'error'));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(WeatherItem);
