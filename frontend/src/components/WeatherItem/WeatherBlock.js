import React from "react";
import "./WeatherBlock.css";

export default ({place, day, temperature, tempType, children, className}) => {

    return (
        <div className={className}>
            {
                place && <div className="weatherPlace">
                    {place}
                </div>
            }
            {
                day && <div className="weatherDay">
                    {day}
                </div>
            }
            {
                temperature && <div className="weatherTemp">
                    {typeof(temperature) === 'object' ? `${temperature.min} - ${temperature.max}` : temperature} {
                        tempType === "Metric" ? "°C" : "°F"
                    }
                </div>
            }
            {
                children
            }
        </div>
    )
};
