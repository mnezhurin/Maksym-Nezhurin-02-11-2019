import React, {
    useState,
    Fragment
} from "react";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import {NavLink} from "react-router-dom";
import "./Menu.css";

export const MenuNav = ({name, children, items}) => {
    const [
        anchorEl,
        setAnchor
        ] = useState(null),
        open = Boolean(anchorEl)
    ;

    const handleChange = event => {setAnchor(event.currentTarget)};
    const handleClose = () => {setAnchor(null)};
    const menuId = name ? name : new Date().getTime();

    return (
        <Fragment>
            <IconButton
                aria-owns={open ? menuId : undefined}
                aria-haspopup="true"
                onClick={handleChange}
                color="inherit"
            >
                {children}
            </IconButton>
            <Menu
                id={menuId}
                className="nav-menu"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                }}
                open={open}
                onClose={handleClose}
            >
                {
                    Object.keys(items).map( (index) => {
                        const item = items[index];
                        return <MenuItem key={item.path} className="nav-menu_item" onClick={handleClose}>
                            <NavLink to={item.path} className="nav-menu_link">{item.label}</NavLink>
                        </MenuItem>
                    })
                }
            </Menu>
        </Fragment>
    )
};