import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';

import {connect} from "react-redux";

import "./Autocomplete.css";
import {findTheCity} from "../../api";
import {addNewCities, callNotify} from "../../actions";

const MyAutocomplete = ({options, label, variant, onAddItem, addOptions, showError}) => {
    const minAmountOfEnteredValues = 3;
    const handlerFindCity = e => {
        const value = e.target.value.toLowerCase();
        if (value.length < minAmountOfEnteredValues) {
            return false;
        }
        findTheCity(value).then(foundCities => {
            addOptions(foundCities);
        }, (e) => showError(`Autocomplete error: ${e.message}`));
    };
    return (
        <Autocomplete
            className="SearchInput"
            options={options}
            getOptionLabel={option => option.LocalizedName}
            onChange={(e, value) => value !== null && onAddItem(value)}
            renderInput={params => (
                <TextField {...params} label={label} variant={variant} fullWidth onChange={handlerFindCity}/>
            )}
        />
    )
};
const mapStateToProps = state => ({
    options: state.cities.cities
});

const mapDispatchToProps = dispatch => ({
    addOptions: options => {
        dispatch(addNewCities(options))
    },
    showError: message => {
        dispatch(callNotify(message, 'error'));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(MyAutocomplete);