import {
    ADD_CITIES
} from "../actions/actionTypes";

const initialState = {
    cities: [
        {
            "Version": 1,
            "Key": "215854",
            "Type": "City",
            "Rank": 31,
            "LocalizedName": "Tel Aviv",
            "Country": {
                "ID": "IL",
                "LocalizedName": "Israel"
            },
            "AdministrativeArea": {
                "ID": "TA",
                "LocalizedName": "Tel Aviv"
            }
        },
        {
            "Version": 1,
            "Key": "323903",
            "Type": "City",
            "Rank": 21,
            "LocalizedName": "Kharkiv",
            "Country": {
                "ID": "UA",
                "LocalizedName": "Ukraine"
            },
            "AdministrativeArea": {
                "ID": "63",
                "LocalizedName": "Kharkiv"
            }
        }
    ]
};

export function cities(state = initialState, action) {
    switch (action.type) {
        case ADD_CITIES:
            return {...state, cities: [...state.cities, ...action.payload]};

        default:
            return state;
    }
}
