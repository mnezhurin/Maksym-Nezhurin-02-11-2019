import {SEND_MESSAGE} from "../actions/actionTypes";

// variant could be success, error, warning or info
const initialState = {
    variant: "success",
    message: "Application was created successful!"
};

export function informer(state = initialState, action) {
    switch (action.type) {
        case SEND_MESSAGE: {
            const {message, variant} = action.payload;
            return {
                message,
                variant: variant ? variant : initialState.variant
            };
        }

        default: {
            return state;
        }
    }
}