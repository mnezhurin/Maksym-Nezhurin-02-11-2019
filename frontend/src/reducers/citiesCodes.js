import {
    CHANGE_CITY_CODE,
    TOGGLE_CITY_TO_FAVORITE
} from "../actions/actionTypes";

const initialState = {
    chooseCity: 215854,
    favorites: []
};

export function codes(state = initialState, action) {
    switch (action.type) {
        case CHANGE_CITY_CODE:
            return {...state, chooseCity: parseInt(action.payload, 10)};

        case TOGGLE_CITY_TO_FAVORITE:
            const favorite = parseInt(action.payload, 10);
            const favoriteState = {...state};
            if (favoriteState.favorites.indexOf(favorite) === -1) {
                const favorites = favoriteState.favorites.concat(favorite);
                return {...favoriteState, favorites};
            } else {
                const favorites = favoriteState.favorites.filter(code => {
                   if(code !== favorite)
                       return code
                });
                return {...favoriteState, favorites};
            }

        default:
            return state;
    }
}
