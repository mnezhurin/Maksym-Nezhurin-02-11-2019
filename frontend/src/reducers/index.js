import {combineReducers} from "redux";
import {cities} from "./cities";
import {informer} from "./informer";
import {codes} from "./citiesCodes";

export default combineReducers({cities, codes, informer})