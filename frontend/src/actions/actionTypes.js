// Cities action types
export const CHANGE_CITY_CODE = "CHANGE_CITY_CODE";
export const ADD_CITIES = "ADD_CITIES";
export const TOGGLE_CITY_TO_FAVORITE = "TOGGLE_CITY_TO_FAVORITE";

// NOTIFY
export const SEND_MESSAGE = "SEND_MESSAGE";