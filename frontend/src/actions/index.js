import {
    ADD_CITIES,
    CHANGE_CITY_CODE, SEND_MESSAGE,
    TOGGLE_CITY_TO_FAVORITE
} from "./actionTypes";

export const addNewCode = code => dispatch => {
    dispatch({type: CHANGE_CITY_CODE, payload: code});
};

export const addFavorite = code => dispatch => {
    dispatch({type: TOGGLE_CITY_TO_FAVORITE, payload: code});
};

export const addNewCities = cities => dispatch => {
    dispatch({type: ADD_CITIES, payload: cities});
};

export const callNotify = (message, variant) => dispatch => {
    dispatch({type: SEND_MESSAGE, payload: {message, variant}});
};